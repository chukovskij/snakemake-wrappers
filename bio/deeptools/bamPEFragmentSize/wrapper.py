"""Snakemake wrapper for bamPEFragmentSize tool (deeptools)."""

__author__ = "Roman Chernyatchik"
__copyright__ = "Copyright (c) 2019 JetBrains"
__email__ = "roman.chernyatchik@jetbrains.com"
__license__ = "MIT"


from snakemake.shell import shell

hist_path = snakemake.output.get("histogram", None)
hist_arg = "--histogram {}".format(hist_path) if hist_path else ""

table_path = snakemake.output.get("table", None)
table_arg = "--table {}".format(table_path) if table_path else ""

threads = snakemake.threads or 1

params_extra = snakemake.params.get("extra", "")
log = snakemake.log_fmt_shell(stdout=True, stderr=True)

shell("bamPEFragmentSize {params_extra} --verbose --bamfiles {snakemake.input.bamfiles}"
      " {hist_arg} {table_arg} --numberOfProcessors {threads} {log}")
